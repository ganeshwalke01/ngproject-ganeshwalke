import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ActivityComponent } from './activity/activity.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';


const routes: Routes = [
{ path :'' , redirectTo :'login', pathMatch :'full'},
{ path : "profile" , component : ProfileComponent},
{ path : "dashboard" , component : DashboardComponent},
{ path : "login" , component : LoginComponent},
{ path : "registration" , component : RegistrationComponent},
{path: "Activity",component:ActivityComponent},
{ path :"**" , component :PagenotfoundComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
